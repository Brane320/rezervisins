

INSERT INTO korisnik ( e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES ('miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES ('tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES ('petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
             
            
INSERT INTO lokal(id,adresa,naziv,tip) VALUES (1, 'Trg Modene 3', 'Petrus Gastro Bar', 'Restoran');
INSERT INTO lokal(id,adresa,naziv,tip) VALUES (2, 'Petrovaradinska tvrdjava BB', 'Museum', 'Nocni klub');
INSERT INTO lokal(id,adresa,naziv,tip) VALUES (3, 'Bulevar Mihajla Pupina 14', 'Pupin Pub', 'Restoran');
INSERT INTO lokal(id,adresa,naziv,tip) VALUES (4, 'Strazilovska 40', 'Kafemat', 'Kafic');
INSERT INTO lokal(id,adresa,naziv,tip) VALUES (5, 'Sutjeska 10', 'Paradiso', 'Nocni klub');


INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (1,4,1,false,'Barski', 1);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (2,4,2,false,'Barski', 1);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (3,4,3,false,'Barski', 1);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (4,4,4,false,'Barski', 1);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (5,6,5,false,'Sedenje', 1);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (6,6,6,false,'Sedenje', 1);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (7,10,7,false,'Sedenje', 1);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (8,10,8,false,'Sedenje', 1);

INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (9,4,1,false,'Barski', 2);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (10,4,2,false,'Barski', 2);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (11,4,3,false,'Barski', 2);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (12,4,4,false,'Barski', 2);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (13,6,5,false,'Visoko sedenje', 2);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (14,6,6,false,'Visoko sedenje', 2);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (15,6,7,false,'Visoko sedenje', 2);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (16,6,8,false,'Visoko sedenje', 2);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (17,10,9,false,'Separe', 2);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (18,10,10,false,'Separe', 2);

INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (19,4,1,false,'Barski', 3);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (20,4,2,false,'Barski', 3);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (21,4,3,false,'Barski', 3);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (22,4,4,false,'Barski', 3);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (23,6,5,false,'Sedenje',3);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (24,6,6,false,'Sedenje',3);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (25,10,7,false,'Sedenje', 3);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (26,10,8,false,'Sedenje', 3);

INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (27,6,1,false,'Sedenje', 4);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (28,6,2,false,'Sedenje', 4);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (29,2,3,false,'Sedenje', 4);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (30,2,4,false,'Sedenje', 4);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (31,6,5,false,'Sedenje',4);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (32,6,6,false,'Sedenje',4);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (33,10,7,false,'Sedenje', 4);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (34,10,8,false,'Sedenje', 4);

INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (35,4,1,false,'Barski', 5);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (36,4,2,false,'Barski', 5);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (37,4,3,false,'Barski', 5);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (38,4,4,false,'Barski', 5);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (39,6,5,false,'Visoko sedenje', 5);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (40,6,6,false,'Visoko sedenje', 5);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (41,8,7,false,'Visoko sedenje', 5);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (42,8,8,false,'Visoko sedenje', 5);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (43,12,9,false,'Separe', 5);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (44,16,10,false,'Separe', 5);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (45,10,11,false,'Separe', 5);
INSERT INTO sto(id,dozvoljen_broj_ljudi,numericki_broj,rezervisan,tip,lokal_id) VALUES (46,10,12,false,'Separe', 5);

INSERT INTO rezervacija(id,broj_osoba,datumivreme,na_ime,korisnik_id,sto_id) VALUES (1,4,'2021-11-15 15:00','Miroslav',1,34);

INSERT INTO komentar(ime_autora,datum_postavljanja,tekst,lokal_id) VALUES ('Miroslav','2021-01-01', 'Odlicna atmosfera,osoblje ljubazno,cene prosecne za ovaj tip lokala',1);
INSERT INTO komentar(ime_autora,datum_postavljanja,tekst,lokal_id) VALUES ('Tamara','2021-06-08', 'Hrana ukusna,velik izbor vina za ljubitelje. Sve preporuke!',1);
INSERT INTO komentar(ime_autora,datum_postavljanja,tekst,lokal_id) VALUES ('Nikola','2021-04-15', 'Najbolji provod u gradu,muzika odlicna!',2);