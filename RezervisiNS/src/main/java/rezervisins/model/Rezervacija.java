package rezervisins.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Rezervacija {
	
	    @Id 
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;  

	    @Column(nullable = false)
	    private String naIme;
	    
	    @Column(nullable = false)
	    private int brojOsoba;
	    
	    @Column
	    private String datumIVreme;
	    
	    @ManyToOne
	    private Sto sto;
	    
	    @ManyToOne
	    private Korisnik korisnik;
	    
	    

		public Rezervacija() {
			
		}
		
		

		public Korisnik getKorisnik() {
			return korisnik;
		}



		public void setKorisnik(Korisnik korisnik) {
			this.korisnik = korisnik;
		}



		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getNaIme() {
			return naIme;
		}

		public void setNaIme(String naIme) {
			this.naIme = naIme;
		}

		public int getBrojOsoba() {
			return brojOsoba;
		}

		public void setBrojOsoba(int brojOsoba) {
			this.brojOsoba = brojOsoba;
		}

		public String getDatumIVreme() {
			return datumIVreme;
		}

		public void setDatumIVreme(String datumIVreme) {
			this.datumIVreme = datumIVreme;
		}

		public Sto getSto() {
			return sto;
		}

		public void setSto(Sto sto) {
			this.sto = sto;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Rezervacija other = (Rezervacija) obj;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			return true;
		}
		
		
	    
	    
	    
	    
}
