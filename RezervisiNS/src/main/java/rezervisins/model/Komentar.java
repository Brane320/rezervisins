package rezervisins.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Komentar {
	
	 @Id 
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long id;
	 
	 @Column
	 private String imeAutora;
	 
	 @Column
	 private String tekst;
	 
	 @ManyToOne
	 private Lokal lokal;
	 
	 @Column
	 private LocalDate datumPostavljanja;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImeAutora() {
		return imeAutora;
	}

	public void setImeAutora(String imeAutora) {
		this.imeAutora = imeAutora;
	}

	public String getTekst() {
		return tekst;
	}

	public void setTekst(String tekst) {
		this.tekst = tekst;
	}

	public Lokal getLokal() {
		return lokal;
	}

	public void setLokal(Lokal lokal) {
		this.lokal = lokal;
	}
	
	

	public LocalDate getDatumPostavljanja() {
		return datumPostavljanja;
	}

	public void setDatumPostavljanja(LocalDate datumPostavljanja) {
		this.datumPostavljanja = datumPostavljanja;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Komentar other = (Komentar) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	 
	 

}
