package rezervisins.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Sto {
	
	 @Id 
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long id;
	
	 @Column(nullable=false)
	 private int numerickiBroj;
	 
	 @Column(nullable=false)
	 private int dozvoljenBrojLjudi;
	 
	 @Column
	 private String tip;
	 
	 @Column
	 private boolean rezervisan;
	 
	 @ManyToOne
	 private Lokal lokal;
	 
	 @OneToMany(mappedBy="sto",cascade = CascadeType.ALL)
	 private List<Rezervacija> rezervacije;

	
	 public Sto() {
	 }
	 
	 


	public List<Rezervacija> getRezervacije() {
		return rezervacije;
	}




	public void setRezervacije(List<Rezervacija> rezervacije) {
		this.rezervacije = rezervacije;
	}




	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public int getNumerickiBroj() {
		return numerickiBroj;
	}


	public void setNumerickiBroj(int numerickiBroj) {
		this.numerickiBroj = numerickiBroj;
	}


	public int getDozvoljenBrojLjudi() {
		return dozvoljenBrojLjudi;
	}


	public void setDozvoljenBrojLjudi(int dozvoljenBrojLjudi) {
		this.dozvoljenBrojLjudi = dozvoljenBrojLjudi;
	}


	public String getTip() {
		return tip;
	}


	public void setTip(String tip) {
		this.tip = tip;
	}


	public boolean isRezervisan() {
		return rezervisan;
	}


	public void setRezervisan(boolean rezervisan) {
		this.rezervisan = rezervisan;
	}


	public Lokal getLokal() {
		return lokal;
	}


	public void setLokal(Lokal lokal) {
		this.lokal = lokal;
	}


	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sto other = (Sto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	 
	 
	 
	 
	 
	 

}
