package rezervisins.support;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rezervisins.model.Komentar;
import rezervisins.web.dto.KomentarDTO;

@Component
public class KomentarToKomentarDTO implements Converter<Komentar,KomentarDTO>{
	
	@Autowired
	private LokalToLokalKomentaraDTO toLokalKomentaraDTO;

	@Override
	public KomentarDTO convert(Komentar source) {
		KomentarDTO dto = new KomentarDTO();
		
		dto.setId(source.getId());
		dto.setImeAutora(source.getImeAutora());
		dto.setTekst(source.getTekst());
		dto.setDatumPostavljanja(source.getDatumPostavljanja().toString());
		dto.setLokal(toLokalKomentaraDTO.convert(source.getLokal()));
		
		return dto;
	}
	
	public List<KomentarDTO> convert(List<Komentar> komentari){
		List<KomentarDTO> dtos = new ArrayList<>();
		
		for(Komentar komentar : komentari) {
			dtos.add(convert(komentar));
		}
		return dtos;
	}
	

}
