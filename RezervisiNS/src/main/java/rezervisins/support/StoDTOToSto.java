package rezervisins.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rezervisins.model.Sto;
import rezervisins.service.LokalService;
import rezervisins.service.StoService;
import rezervisins.web.dto.StoDTO;

@Component
public class StoDTOToSto implements Converter<StoDTO,Sto> {
	
	@Autowired
	private StoService stoService;
	
	@Autowired
	private LokalService lokalService;

	@Override
	public Sto convert(StoDTO dto) {
		Sto sto;
		
		if(dto.getId() == null) {
			sto = new Sto();
		} else {
			sto = stoService.getOne(dto.getId());
		}
		if(sto != null) {
			sto.setDozvoljenBrojLjudi(dto.getDozvoljenBrojLjudi());
			sto.setNumerickiBroj(dto.getNumerickiBroj());
			sto.setRezervisan(dto.isRezervisan());
			sto.setTip(dto.getTip());
			
			if(dto.getLokal() != null) {
			sto.setLokal(lokalService.getOne(dto.getLokal().getId()));
			}
		}
		return sto;
	
	}
	
	public List<Sto> convert(List<StoDTO> dto){
		List<Sto> stolovi = new ArrayList<>();
		
		for(StoDTO stoDTO : dto) {
			stolovi.add(convert(stoDTO));
		}
		return stolovi;
	}

}
