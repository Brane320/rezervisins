package rezervisins.support;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rezervisins.model.Korisnik;
import rezervisins.web.dto.KorisnikRez;

@Component
public class KorisnikToKorisnikRez implements Converter<Korisnik,KorisnikRez> {

	@Override
	public KorisnikRez convert(Korisnik source) {
		
		KorisnikRez korisnikRez = new KorisnikRez();
		
		korisnikRez.setId(source.getId());
		korisnikRez.seteMail(source.geteMail());
		korisnikRez.setIme(source.getIme());
		korisnikRez.setPrezime(source.getPrezime());
		korisnikRez.setKorisnickoIme(source.getKorisnickoIme());
		
		return korisnikRez;
	}
	
	

}
