package rezervisins.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rezervisins.model.Lokal;
import rezervisins.web.dto.LokalDTO;

@Component
public class LokalToLokalDTO implements Converter<Lokal,LokalDTO> {
	
	@Autowired
	private StoToStoDTO toStoDTO;
	
	@Autowired
	private KomentarToKomentarDTO toKomentarDTO;

	@Override
	public LokalDTO convert(Lokal source) {
		LokalDTO dto = new LokalDTO();
		
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		dto.setAdresa(source.getAdresa());
		dto.setTip(source.getTip());
		if(source.getStolovi() != null) {
		dto.setStolovi(toStoDTO.convert(source.getStolovi()));
		}
		
		if(source.getKomentari() != null) {
			dto.setKomentari(toKomentarDTO.convert(source.getKomentari()));
		}
		
		return dto;
	}
	
	public List<LokalDTO> convert(List<Lokal> lokali){
		List<LokalDTO> dto = new ArrayList<>();
		
		for(Lokal lokal : lokali) {
			dto.add(convert(lokal));
		}
		return dto;
	}

}
