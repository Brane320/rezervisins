package rezervisins.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rezervisins.model.Korisnik;
import rezervisins.web.dto.KorisnikDTO;



@Component
public class KorisnikToKorisnikDto implements Converter<Korisnik, KorisnikDTO>{
	
	@Autowired
	private RezervacijaToRezervacijaDTO toRezDTO;

   

    @Override
    public KorisnikDTO convert(Korisnik korisnik) {
        KorisnikDTO korisnikDTO = new KorisnikDTO();

        korisnikDTO.setId(korisnik.getId());
        korisnikDTO.seteMail(korisnik.geteMail());
        korisnikDTO.setIme(korisnik.getIme());
        korisnikDTO.setPrezime(korisnik.getPrezime());
        korisnikDTO.setKorisnickoIme(korisnik.getKorisnickoIme());
        
        if(korisnik.getRezervacije() != null) {
        	korisnikDTO.setRezervacije(toRezDTO.convert(korisnik.getRezervacije()));
        }
        

        return korisnikDTO;
    }

    public List<KorisnikDTO> convert(List<Korisnik> korisnici){
        List<KorisnikDTO> korisnikDTOS = new ArrayList<>();

        for(Korisnik k : korisnici) {
            KorisnikDTO dto = convert(k);
            korisnikDTOS.add(dto);
        }

        return korisnikDTOS;
    }
}
