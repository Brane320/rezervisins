package rezervisins.support;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rezervisins.model.Komentar;
import rezervisins.service.KomentarService;
import rezervisins.service.LokalService;
import rezervisins.web.dto.KomentarDTO;

@Component
public class KomentarDTOToKomentar implements Converter<KomentarDTO,Komentar> {
	
	@Autowired
	private KomentarService komentarService;
	
	@Autowired
	private LokalService lokalService;
	
	private LocalDate getLocalDate(String datumS) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate datum = LocalDate.parse(datumS, formatter);
     
        return datum;
    }

	@Override
	public Komentar convert(KomentarDTO source) {
		
		Komentar komentar;
		
		if(source.getId() != null) {
			komentar = komentarService.getOne(source.getId());
		} else {
			komentar = new Komentar();
		}
		
		if(komentar != null) {
			komentar.setImeAutora(source.getImeAutora());
			komentar.setTekst(source.getTekst());
			komentar.setDatumPostavljanja(getLocalDate(source.getDatumPostavljanja()));
			
			if(source.getLokal() != null) {
				komentar.setLokal(lokalService.getOne(source.getLokal().getId()));
			}
			
		}
		return komentar;
		
	}
		
		public List<Komentar> convert(List<KomentarDTO> dtos){
			List<Komentar> komentari = new ArrayList<>();
			
			for(KomentarDTO dto : dtos) {
				komentari.add(convert(dto));
			}
			return komentari;
		}
		
		
		
	}


