package rezervisins.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rezervisins.model.Sto;
import rezervisins.web.dto.StoDTO;

@Component
public class StoToStoDTO implements Converter<Sto,StoDTO>{

	@Autowired
	LokalToLokalStolaDTO toLokalStolaDTO;
	
	@Override
	public StoDTO convert(Sto source) {
		StoDTO dto = new StoDTO();
		
		dto.setId(source.getId());
		dto.setDozvoljenBrojLjudi(source.getDozvoljenBrojLjudi());
		dto.setNumerickiBroj(source.getNumerickiBroj());
		dto.setRezervisan(source.isRezervisan());
		dto.setTip(source.getTip());
		dto.setLokal(toLokalStolaDTO.convert(source.getLokal()));
		return dto;
	}
	
	public List<StoDTO> convert(List<Sto> stolovi){
		List<StoDTO> dto = new ArrayList<>();
		
		for(Sto sto : stolovi) {
			dto.add(convert(sto));
		}
		return dto;
	}
	
	

}
