package rezervisins.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rezervisins.model.Korisnik;
import rezervisins.model.Rezervacija;
import rezervisins.service.KorisnikService;
import rezervisins.service.RezervacijaService;
import rezervisins.web.dto.RezervacijaDTO;

@Component
public class RezervacijaDTOToRezervacija implements Converter<RezervacijaDTO,Rezervacija>{
	
	@Autowired
	private RezervacijaService rezervacijaService;
	
	@Autowired
	private StoDTOToSto toSto;
	
	
	@Autowired
	private KorisnikService korisnikService;
	
	@Override
	public Rezervacija convert(RezervacijaDTO dto) {
		
		Rezervacija rezervacija;
		
		if(dto.getId() == null) {
			rezervacija = new Rezervacija();
		} else {
			rezervacija = rezervacijaService.getOne(dto.getId());
		}
		
		if(rezervacija != null) {
			rezervacija.setBrojOsoba(dto.getBrojOsoba());
			rezervacija.setDatumIVreme(dto.getDatumIVreme());
			rezervacija.setNaIme(dto.getNaIme());
			rezervacija.setSto(toSto.convert(dto.getSto()));
			 
			Korisnik korisnik = korisnikService.getOne(dto.getKorisnik().getId());
			
			rezervacija.setKorisnik(korisnik);
			
		}
		return rezervacija;
		
	}
	
	public List<Rezervacija> convert(List<RezervacijaDTO> dto){
		List<Rezervacija> rez = new ArrayList<>();
		
		for(RezervacijaDTO rezDto : dto) {
			rez.add(convert(rezDto));
		}
		
		return rez;
	}

}
