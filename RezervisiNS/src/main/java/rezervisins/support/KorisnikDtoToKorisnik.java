package rezervisins.support;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rezervisins.model.Korisnik;
import rezervisins.service.KorisnikService;
import rezervisins.web.dto.KorisnikDTO;


@Component
public class KorisnikDtoToKorisnik implements Converter<KorisnikDTO, Korisnik> {

    @Autowired
    private KorisnikService korisnikService;
    
    @Autowired
    private RezervacijaDTOToRezervacija toRez;


    @Override
    public Korisnik convert(KorisnikDTO korisnikDTO) {
        Korisnik korisnik = null;
       
        if(korisnikDTO.getId() == null) {
        	korisnik = new Korisnik();
        } else {
        	korisnik = korisnikService.getOne(korisnikDTO.getId());
        }

        
        
        
        if(korisnik != null) {
        	
        	korisnik.setKorisnickoIme(korisnikDTO.getKorisnickoIme());
        	korisnik.seteMail(korisnikDTO.geteMail());
        	korisnik.setIme(korisnikDTO.getIme());
        	korisnik.setPrezime(korisnikDTO.getPrezime());
        	
        	if(korisnikDTO.getRezervacije() != null) {
        		
        		korisnik.setRezervacije(toRez.convert(korisnikDTO.getRezervacije()));
        	}
        	
        
        }
        

        return korisnik;
    }

}
