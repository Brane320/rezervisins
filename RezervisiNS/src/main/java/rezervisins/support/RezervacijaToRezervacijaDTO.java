package rezervisins.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rezervisins.model.Korisnik;
import rezervisins.model.Rezervacija;
import rezervisins.service.KorisnikService;
import rezervisins.web.dto.RezervacijaDTO;

@Component
public class RezervacijaToRezervacijaDTO implements Converter<Rezervacija,RezervacijaDTO>{
	
	@Autowired
	private StoToStoDTO toStoDTO;
	
	
	
	@Autowired
	private KorisnikToKorisnikRez toKorisnikRez;
	
	@Autowired
	private KorisnikService korisnikService;
	


	@Override
	public RezervacijaDTO convert(Rezervacija source) {
		RezervacijaDTO dto = new RezervacijaDTO();
		
		
		dto.setId(source.getId());
		dto.setBrojOsoba(source.getBrojOsoba());
		dto.setDatumIVreme(source.getDatumIVreme());
		dto.setNaIme(source.getNaIme());
		dto.setSto(toStoDTO.convert(source.getSto()));
		
		Korisnik korisnik = korisnikService.getOne(source.getKorisnik().getId());
		
		dto.setKorisnik(toKorisnikRez.convert(korisnik));
		
		return dto;
	}
	
	public List<RezervacijaDTO> convert(List<Rezervacija> rezervacije){
		List<RezervacijaDTO> dto = new ArrayList<>();
		
		for(Rezervacija rezervacija : rezervacije) {
			dto.add(convert(rezervacija));

		}
		return dto;
	}
	

}
