package rezervisins.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rezervisins.model.Lokal;
import rezervisins.service.LokalService;
import rezervisins.web.dto.LokalDTO;

@Component
public class LokalDTOToLokal implements Converter<LokalDTO,Lokal> {
	
	@Autowired
	private LokalService lokalService;
	
	@Autowired
	private StoDTOToSto toSto;
	
	@Autowired
	private KomentarDTOToKomentar toKomentar;

	@Override
	public Lokal convert(LokalDTO dto) {
		Lokal lokal;
		
		if(dto.getId() == null) {
			lokal = new Lokal();
		} else {
			lokal = lokalService.getOne(dto.getId());
		}
		
		if(lokal != null) {
			lokal.setAdresa(dto.getAdresa());
			lokal.setNaziv(dto.getNaziv());
			lokal.setTip(dto.getTip());
			
			if(dto.getStolovi() != null) {
			lokal.setStolovi(toSto.convert(dto.getStolovi()));
			}
			
			if(dto.getKomentari() != null) {
				lokal.setKomentari(toKomentar.convert(dto.getKomentari()));
			}
		
		}
		return lokal;
		
	}

}
