package rezervisins.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rezervisins.model.Lokal;
import rezervisins.web.dto.LokalKomentaraDTO;

@Component
public class LokalToLokalKomentaraDTO implements Converter<Lokal,LokalKomentaraDTO> {

	@Override
	public LokalKomentaraDTO convert(Lokal source) {
		
		LokalKomentaraDTO dto = new LokalKomentaraDTO();
		
		dto.setId(source.getId());
		dto.setAdresa(source.getAdresa());
		dto.setNaziv(source.getNaziv());
		dto.setTip(source.getTip());
		
		return dto;
	}
	
	private List<LokalKomentaraDTO> convert(List<Lokal> lokali){
		List<LokalKomentaraDTO> dtos = new ArrayList<>();
		
		for(Lokal lokal : lokali) {
			dtos.add(convert(lokal));
		}
		return dtos;
	}

}
