package rezervisins.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rezervisins.model.Lokal;
import rezervisins.web.dto.LokalStolaDTO;

@Component
public class LokalToLokalStolaDTO implements Converter<Lokal,LokalStolaDTO>{

	@Override
	public LokalStolaDTO convert(Lokal lokal) {
		LokalStolaDTO lokalStola = new LokalStolaDTO();
		
		lokalStola.setId(lokal.getId());
		lokalStola.setAdresa(lokal.getAdresa());
		lokalStola.setNaziv(lokal.getNaziv());
		lokalStola.setTip(lokal.getTip());
		
		return lokalStola;
		
		
	}
	
	public List<LokalStolaDTO> convert (List<Lokal> lokali){
		List<LokalStolaDTO> dto = new ArrayList<>();
		
		for(Lokal lokal : lokali) {
			dto.add(convert(lokal));
		}
		return dto;
	}

}
