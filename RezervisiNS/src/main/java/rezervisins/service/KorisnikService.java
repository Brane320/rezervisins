package rezervisins.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import rezervisins.model.Korisnik;
import rezervisins.web.dto.KorisnikPromenaLozinkeDTO;



public interface KorisnikService {
	Optional<Korisnik> findOne(Long id);

    List<Korisnik> findAll();

    Page<Korisnik> findAll(int brojStranice);

    Korisnik save(Korisnik korisnik);
    
    Korisnik getOne(Long id);

    void delete(Long id);

    Optional<Korisnik> findbyKorisnickoIme(String korisnickoIme);

    boolean changePassword(Long id, KorisnikPromenaLozinkeDTO korisnikPromenaLozinkeDto);
    
    Korisnik findByKorisnickoIme(String korisnickoIme);

}
