package rezervisins.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import rezervisins.model.Lokal;
import rezervisins.repository.LokalRepository;
import rezervisins.service.LokalService;
@Service
public class JpaLokalService implements LokalService {
	
	@Autowired
	private LokalRepository lokalRepository;

	@Override
	public List<Lokal> getAll() {
		return lokalRepository.findAll();
	}

	@Override
	public Page<Lokal> findAll(int brojStranice) {
		return lokalRepository.findAll(PageRequest.of(brojStranice, 3));
	}

	@Override
	public Lokal getOne(Long id) {
		return lokalRepository.getOne(id);
	}

	@Override
	public Optional<Lokal> findOneById(Long id) {
		return lokalRepository.findById(id);
	}

	@Override
	public Lokal save(Lokal lokal) {
		return lokalRepository.save(lokal);
	}

	@Override
	public Lokal update(Lokal lokal) {
		return lokalRepository.save(lokal);
	}

	@Override
	public Lokal delete(Long id) {
		Optional<Lokal> lokal = lokalRepository.findById(id);
		
		if(lokal.isPresent()) {
			lokalRepository.deleteById(id);
			return lokal.get();
		}
		return null;
	}

	@Override
	public List<Lokal> find(String naziv) {
		if(naziv == null) {
			naziv = "";
		}
		return lokalRepository.findByNazivIgnoreCaseContains(naziv);
	}

}
