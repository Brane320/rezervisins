package rezervisins.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rezervisins.model.Sto;
import rezervisins.repository.StoRepository;
import rezervisins.service.StoService;

@Service
public class JpaStoService implements StoService{
	
	@Autowired
	private StoRepository stoRepository;

	@Override
	public List<Sto> getAll() {
		return stoRepository.findAll();
	}

	@Override
	public Sto getOne(Long id) {
		return stoRepository.getOne(id);
	}

	@Override
	public Optional<Sto> findById(Long id) {
		return stoRepository.findById(id);
	}

	@Override
	public Sto save(Sto sto) {
	
		return stoRepository.save(sto);
	}

	@Override
	public Sto delete(Long id) {
		Optional<Sto> sto = stoRepository.findById(id);
		
		if(sto.isPresent()) {
			stoRepository.deleteById(id);
			return sto.get();
		}
		return null;
	}

	@Override
	public Sto update(Sto sto) {
		return stoRepository.save(sto);
	}

}
