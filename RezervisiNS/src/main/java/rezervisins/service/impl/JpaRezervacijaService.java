package rezervisins.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rezervisins.model.Korisnik;
import rezervisins.model.Rezervacija;
import rezervisins.model.Sto;
import rezervisins.repository.RezervacijaRepository;
import rezervisins.service.RezervacijaService;
import rezervisins.service.StoService;

@Service
public class JpaRezervacijaService implements RezervacijaService {
	
	@Autowired
	private RezervacijaRepository rezervacijaRepository;
	
	@Autowired
	private StoService stoService;
	
	

	@Override
	public Rezervacija rezervisi(Rezervacija rezervacija) {
		if(rezervacija.getBrojOsoba() <= rezervacija.getSto().getDozvoljenBrojLjudi()) {
			rezervacija.getSto().setRezervisan(true);
			Sto sto = rezervacija.getSto();
			stoService.update(sto);
			
			Korisnik korisnik = rezervacija.getKorisnik();
			
			List<Rezervacija> rez = new ArrayList<>();
			
			rez.add(rezervacija);
			
			korisnik.setRezervacije(rez);
			
			
			
			
			return rezervacijaRepository.save(rezervacija);
			
		
		}
		return null;
	}

	@Override
	public Rezervacija getOne(Long id) {
		return rezervacijaRepository.getOne(id);
	}

	

	@Override
	public Rezervacija delete(Long id) {
		Optional<Rezervacija> rez = rezervacijaRepository.findById(id);
		
		if(rez.isPresent()) {
			rezervacijaRepository.deleteById(id);
			return rez.get();
		}
		return null;
	}

	@Override
	public Optional<Rezervacija> findById(Long id) {
		return rezervacijaRepository.findById(id);
	}

}
