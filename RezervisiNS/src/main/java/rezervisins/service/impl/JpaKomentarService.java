package rezervisins.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rezervisins.model.Komentar;
import rezervisins.repository.KomentarRepository;
import rezervisins.service.KomentarService;
@Service
public class JpaKomentarService implements KomentarService {
	
	@Autowired
	private KomentarRepository komentarRepository;

	@Override
	public List<Komentar> findAll() {
		return komentarRepository.findAll();
	}

	@Override
	public Komentar getOne(Long id) {
		return komentarRepository.getOne(id);
	}

	@Override
	public Komentar delete(Long id) {
		Optional<Komentar> komentar = komentarRepository.findById(id);
		
		if(komentar.isPresent()) {
			komentarRepository.deleteById(id);
			return komentar.get();
		} else {
			return null;
		}
	}

	@Override
	public Komentar save(Komentar komentar) {
		komentar.setDatumPostavljanja(LocalDate.now());
		return komentarRepository.save(komentar);
	}

	@Override
	public Optional<Komentar> findById(Long id) {
		return komentarRepository.findById(id);
	}

}
