package rezervisins.service;

import java.util.Optional;

import rezervisins.model.Rezervacija;

public interface RezervacijaService {
	
	Rezervacija rezervisi(Rezervacija rezervacija);
	
	Rezervacija getOne(Long id);
	
	Optional<Rezervacija> findById(Long id);
	
	Rezervacija delete(Long id);

}
