package rezervisins.service;

import java.util.List;
import java.util.Optional;

import rezervisins.model.Komentar;

public interface KomentarService {
	
	List<Komentar> findAll();
	
	Komentar getOne(Long id);
	
	Komentar delete(Long id);
	
	Komentar save(Komentar komentar);
	
	Optional<Komentar> findById(Long id);

}
