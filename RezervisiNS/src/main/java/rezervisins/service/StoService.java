package rezervisins.service;

import java.util.List;
import java.util.Optional;

import rezervisins.model.Sto;

public interface StoService {

	List<Sto> getAll();
	
	Sto getOne(Long id);
	
	Optional<Sto> findById(Long id);
	
	Sto save(Sto sto);
	
	Sto delete(Long id);
	
	Sto update(Sto sto);
	
}
