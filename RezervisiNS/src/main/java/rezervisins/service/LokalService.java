package rezervisins.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import rezervisins.model.Lokal;

public interface LokalService {
	
	List<Lokal> getAll();
	
	Page<Lokal> findAll(int brojStranice);
	
	Lokal getOne(Long id);
	
	Optional<Lokal> findOneById(Long id);
	
	Lokal save(Lokal lokal);
	
	Lokal update(Lokal lokal);
	
	Lokal delete(Long id);
	
	List<Lokal> find(String naziv);

}
