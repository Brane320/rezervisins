package rezervisins.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rezervisins.model.Lokal;

@Repository
public interface LokalRepository extends JpaRepository<Lokal,Long> {
	
	List<Lokal> findByNazivIgnoreCaseContains (String naziv);

}
