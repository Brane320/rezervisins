package rezervisins.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rezervisins.model.Sto;

@Repository
public interface StoRepository extends JpaRepository<Sto,Long>{

}
