package rezervisins.web.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rezervisins.model.Sto;
import rezervisins.service.StoService;
import rezervisins.support.StoDTOToSto;
import rezervisins.support.StoToStoDTO;
import rezervisins.web.dto.StoDTO;


@RestController
@RequestMapping(value = "/api/stolovi", produces = MediaType.APPLICATION_JSON_VALUE)
public class StoController {
	
	@Autowired
	private StoService stoService;
	
	@Autowired
	private StoToStoDTO toStoDTO;
	
	@Autowired
	private StoDTOToSto toSto;
	
	 @GetMapping(value="/{id}")
		public ResponseEntity<StoDTO> getById(@PathVariable Long id){
			Optional<Sto> sto = stoService.findById(id);
			
			if(sto.isPresent()) {
				return new ResponseEntity<>(toStoDTO.convert(sto.get()),HttpStatus.OK);
			}else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			
		}

	    @PreAuthorize("hasRole('ADMIN')")
	    @PostMapping
	    public ResponseEntity<StoDTO> create(@Valid @RequestBody StoDTO stoDTO){
	    	Sto sto = toSto.convert(stoDTO);
	    	sto.setRezervisan(false);
	    	
	    	Sto sacuvan = stoService.save(sto);
	    	
	    	
	    	
	    	return new ResponseEntity<>(toStoDTO.convert(sacuvan),HttpStatus.CREATED);
	    }
}
