package rezervisins.web.controller;

import java.time.LocalDate;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rezervisins.model.Komentar;
import rezervisins.service.KomentarService;
import rezervisins.support.KomentarDTOToKomentar;
import rezervisins.support.KomentarToKomentarDTO;
import rezervisins.web.dto.KomentarDTO;

@RestController
@RequestMapping(value = "/api/komentari", produces = MediaType.APPLICATION_JSON_VALUE)
public class KomentarController {
	
	@Autowired
	private KomentarService service;
	
	@Autowired
	private KomentarToKomentarDTO toKomentarDTO;
	
	@Autowired
	private KomentarDTOToKomentar toKomentar;
	
	
	@GetMapping(value="/{id}")
	public ResponseEntity<KomentarDTO> getById(@PathVariable Long id){
		Optional<Komentar> komentar = service.findById(id);
		
		if(komentar.isPresent()) {
			return new ResponseEntity<>(toKomentarDTO.convert(komentar.get()), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		
	}
	
	@PostMapping
	public ResponseEntity<KomentarDTO> create(@Valid @RequestBody KomentarDTO komentarDTO){
		komentarDTO.setDatumPostavljanja(LocalDate.now().toString());
		Komentar komentar = toKomentar.convert(komentarDTO);
		
		
		Komentar sacuvan = service.save(komentar);
		
		return new ResponseEntity<>(toKomentarDTO.convert(sacuvan),HttpStatus.OK);
		
	}
		
	

}
