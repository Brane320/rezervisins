package rezervisins.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rezervisins.model.Rezervacija;
import rezervisins.service.RezervacijaService;
import rezervisins.support.RezervacijaDTOToRezervacija;
import rezervisins.support.RezervacijaToRezervacijaDTO;
import rezervisins.web.dto.RezervacijaDTO;

@RestController
@RequestMapping(value = "/api/rezervacije", produces = MediaType.APPLICATION_JSON_VALUE)
public class RezervacijaController {
	
	@Autowired
	private RezervacijaService rezervacijaService;
	
	@Autowired
	private RezervacijaToRezervacijaDTO toRezervacijaDTO;
	
	
	@Autowired
	private RezervacijaDTOToRezervacija toRezervacija;
	
	
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RezervacijaDTO> rezervisi(@Valid @RequestBody RezervacijaDTO rezervacijaDTO){
       Rezervacija rezervacija = toRezervacija.convert(rezervacijaDTO);
       
      
       Rezervacija sacuvana = rezervacijaService.rezervisi(rezervacija);
      

        return new ResponseEntity<>(toRezervacijaDTO.convert(sacuvana), HttpStatus.CREATED);
    }
	
	@DeleteMapping("/{id}")
	public ResponseEntity<RezervacijaDTO> delete(@PathVariable Long id){
		Rezervacija obrisana = rezervacijaService.delete(id);
		
		if(obrisana != null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}


}
