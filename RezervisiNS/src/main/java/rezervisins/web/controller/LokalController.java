package rezervisins.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rezervisins.model.Lokal;
import rezervisins.service.LokalService;
import rezervisins.support.LokalDTOToLokal;
import rezervisins.support.LokalToLokalDTO;
import rezervisins.web.dto.LokalDTO;



@RestController
@RequestMapping(value = "/api/lokali", produces = MediaType.APPLICATION_JSON_VALUE)
public class LokalController {
	
	@Autowired
	private LokalService lokalService;
	
	@Autowired
	private LokalToLokalDTO toLokalDTO;
	
	@Autowired
	private LokalDTOToLokal toLokal;
	
	    @PreAuthorize("permitAll()")
	    @GetMapping
		public ResponseEntity<List<LokalDTO>> getAll(@RequestParam(defaultValue="0") int brojStranice){
			Page<Lokal> svi = lokalService.findAll(brojStranice);
			HttpHeaders headers = new HttpHeaders();
			headers.set("Total-Pages", svi.getTotalPages() + "");
			
			return new ResponseEntity<>(toLokalDTO.convert(svi.getContent()),headers,HttpStatus.OK);
			
		}
	    
	    @GetMapping(value="/{id}")
		public ResponseEntity<LokalDTO> getById(@PathVariable Long id){
			Optional<Lokal> lokal = lokalService.findOneById(id);
			
			if(lokal.isPresent()) {
				return new ResponseEntity<>(toLokalDTO.convert(lokal.get()),HttpStatus.OK);
			}else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			
		}
	    @PreAuthorize("hasRole('ADMIN')")
	    @PostMapping
	    public ResponseEntity<LokalDTO> create(@Valid @RequestBody LokalDTO lokalDTO){
	    	Lokal lokal = toLokal.convert(lokalDTO);
	    	
	    	Lokal sacuvan = lokalService.save(lokal);
	    	
	    	return new ResponseEntity<>(toLokalDTO.convert(sacuvan),HttpStatus.CREATED);
	    }
	    
	   

	    @PreAuthorize("permitAll()")
	    @GetMapping("/pretraga")
	    public ResponseEntity<List<LokalDTO>> pretraga(@RequestParam(required=false) String naziv){
	    	List<Lokal> rezultat = lokalService.find(naziv);
	    	
	    	
	    	return new ResponseEntity<>(toLokalDTO.convert(rezultat),HttpStatus.OK);
	    }
	    
	    @PreAuthorize("hasRole('ADMIN')")
	    @DeleteMapping("/{id}")
	    public ResponseEntity<LokalDTO> delete(@PathVariable Long id){
	    	Lokal obrisan = lokalService.delete(id);
	    	
	    	if(obrisan != null) {
	    		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    	} else {
	    		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    	}
	    	
	    }

}
