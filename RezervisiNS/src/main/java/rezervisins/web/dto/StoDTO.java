package rezervisins.web.dto;



public class StoDTO {
	
	 
	 private Long id;
	
	 
	 private int numerickiBroj;
	 
	 
	 private int dozvoljenBrojLjudi;
	 
	
	 private String tip;
	 
	
	 private boolean rezervisan;
	 
	 private LokalStolaDTO lokal;
	 
	 


	public LokalStolaDTO getLokal() {
		return lokal;
	}


	public void setLokal(LokalStolaDTO lokal) {
		this.lokal = lokal;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public int getNumerickiBroj() {
		return numerickiBroj;
	}


	public void setNumerickiBroj(int numerickiBroj) {
		this.numerickiBroj = numerickiBroj;
	}


	public int getDozvoljenBrojLjudi() {
		return dozvoljenBrojLjudi;
	}


	public void setDozvoljenBrojLjudi(int dozvoljenBrojLjudi) {
		this.dozvoljenBrojLjudi = dozvoljenBrojLjudi;
	}


	public String getTip() {
		return tip;
	}


	public void setTip(String tip) {
		this.tip = tip;
	}


	public boolean isRezervisan() {
		return rezervisan;
	}


	public void setRezervisan(boolean rezervisan) {
		this.rezervisan = rezervisan;
	}
	 
	 
	 

}
