package rezervisins.web.dto;

public class KomentarDTO {
	
	 
	 private Long id;
	 
	 
	 private String imeAutora;
	 
	 
	 private String tekst;
	 
	 private String datumPostavljanja;
	 
	 
	 private LokalKomentaraDTO lokal;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getImeAutora() {
		return imeAutora;
	}


	public void setImeAutora(String imeAutora) {
		this.imeAutora = imeAutora;
	}


	public String getTekst() {
		return tekst;
	}


	public void setTekst(String tekst) {
		this.tekst = tekst;
	}


	public LokalKomentaraDTO getLokal() {
		return lokal;
	}


	public void setLokal(LokalKomentaraDTO lokal) {
		this.lokal = lokal;
	}


	public String getDatumPostavljanja() {
		return datumPostavljanja;
	}


	public void setDatumPostavljanja(String datumPostavljanja) {
		this.datumPostavljanja = datumPostavljanja;
	}
	
	


	
	 
	 
	 
	 

}
