package rezervisins.web.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class RezervacijaDTO {
	
	 
	    private Long id;  

	    @NotEmpty		
	    private String naIme;
	    	 
	    @NotNull
	    private int brojOsoba;
	    
	   
	    private String datumIVreme;
	    
	    
	    private StoDTO sto;
	    
	    private KorisnikRez korisnik;
	    
	    


		public KorisnikRez getKorisnik() {
			return korisnik;
		}


		public void setKorisnik(KorisnikRez korisnik) {
			this.korisnik = korisnik;
		}


		public Long getId() {
			return id;
		}


		public void setId(Long id) {
			this.id = id;
		}


		public String getNaIme() {
			return naIme;
		}


		public void setNaIme(String naIme) {
			this.naIme = naIme;
		}


		public int getBrojOsoba() {
			return brojOsoba;
		}


		public void setBrojOsoba(int brojOsoba) {
			this.brojOsoba = brojOsoba;
		}


		public String getDatumIVreme() {
			return datumIVreme;
		}


		public void setDatumIVreme(String datumIVreme) {
			this.datumIVreme = datumIVreme;
		}


		public StoDTO getSto() {
			return sto;
		}


		public void setSto(StoDTO sto) {
			this.sto = sto;
		}
	    
	    

}
