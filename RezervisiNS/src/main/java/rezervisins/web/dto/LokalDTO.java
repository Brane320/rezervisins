package rezervisins.web.dto;

import java.util.List;

public class LokalDTO {
	  
	    private Long id;
	    
	    
	    private String naziv;
	    
	   
	    private String adresa;
	    
	    
	    private String tip;
	    
	    
	    private List<StoDTO> stolovi;
	    
	    private List<KomentarDTO> komentari;


		public Long getId() {
			return id;
		}


		public void setId(Long id) {
			this.id = id;
		}


		public String getNaziv() {
			return naziv;
		}


		public void setNaziv(String naziv) {
			this.naziv = naziv;
		}


		public String getAdresa() {
			return adresa;
		}


		public void setAdresa(String adresa) {
			this.adresa = adresa;
		}


		public String getTip() {
			return tip;
		}


		public void setTip(String tip) {
			this.tip = tip;
		}


		public List<StoDTO> getStolovi() {
			return stolovi;
		}


		public void setStolovi(List<StoDTO> stolovi) {
			this.stolovi = stolovi;
		}


		public List<KomentarDTO> getKomentari() {
			return komentari;
		}


		public void setKomentari(List<KomentarDTO> komentari) {
			this.komentari = komentari;
		}

	    

}
