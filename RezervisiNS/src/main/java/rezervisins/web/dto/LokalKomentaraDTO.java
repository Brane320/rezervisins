package rezervisins.web.dto;

public class LokalKomentaraDTO {
	
	 
	    private Long id;
	    
	   
	    private String naziv;
	   
	    private String adresa;
	    
	    private String tip;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getNaziv() {
			return naziv;
		}

		public void setNaziv(String naziv) {
			this.naziv = naziv;
		}

		public String getAdresa() {
			return adresa;
		}

		public void setAdresa(String adresa) {
			this.adresa = adresa;
		}

		public String getTip() {
			return tip;
		}

		public void setTip(String tip) {
			this.tip = tip;
		}
	    
	    
	    
	    
	    
	   
	    

}
