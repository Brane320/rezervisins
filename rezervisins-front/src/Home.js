import React from 'react'
import novisad from './nspanoramic.mp4'
import Typewriter from 'typewriter-effect'




class Home extends React.Component {
  render() {
    return (
      <div>
      <div>
        <video autoPlay loop muted id='video'
        style={{
          position : "absolute",
          width : "100%",
          left : "50%",
          top : "50%",
          height : "100%",
          objectFit : "cover",
          zIndex : "-1",
          transform : "translate(-50%,-50%)"
        }}>
        <source src={novisad} type='video/mp4'/>
        </video>
      </div>  
    <div style={{fontSize : "58px", textAlign : "center" , fontFamily : "courier new"}}>

        <p>Dobrodošli!</p>
  
    </div>

  <div style={{fontSize : "28px", textAlign : "center" , fontFamily : "courier new"}}>

<Typewriter
  options={{
    autoStart: true,
    cursor : "",
    delay : 50
  }}
  onInit={(typewriter) => {
    typewriter.typeString("Rezervišite mesto u bilo kom lokalu u gradu na našoj stranici!")
    .start()
  }}
/>


  </div>
</div>
    )
  
  }
}

export default Home;