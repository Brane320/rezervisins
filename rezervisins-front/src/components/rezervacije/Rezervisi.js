import React from 'react';
import TestAxios from '../../apis/TestAxios';
import {Form, Button, Row, Col, InputGroup } from "react-bootstrap";
class Rezervisi extends React.Component {

    constructor(props){
        super(props);

    let sto = {
        id : -1,
        numerickiBroj : -1,
        dozvoljenBrojLjudi : -1,
        tip : "",
        rezervisan : null
    }

    this.state = {id : 0, naIme : "", brojOsoba : 0, datumIVreme : "",sto : sto,korisnik : {},rezervacije : []};
}

componentDidMount(){
    this.getStoById(this.props.match.params.id);
    this.setujKorisnika();
    
 
}

naImeInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let naIme = this.state.naIme;
    naIme = value;

    this.setState({ naIme:naIme });
  }

  brojOsobaInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let brojOsoba = this.state.brojOsoba;
    brojOsoba = value;

    this.setState({ brojOsoba : brojOsoba });
  }

  datumIVremeInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let datumIVreme = this.state.datumIVreme;
    datumIVreme = value;

    this.setState({ datumIVreme : datumIVreme });
  }


getStoById(stoId) {
    TestAxios.get('/stolovi/' + stoId)
    .then(res => {
        // handle success
        console.log(res.data);
        let sto = {
            id : res.data.id,
            numerickiBroj : res.data.numerickiBroj,
            dozvoljenBrojLjudi : res.data.dozvoljenBrojLjudi,
            tip : res.data.tip,
            rezervisan : res.data.rezervisan
            


        }
        this.setState({sto : sto});
        
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Sto nije dobijen')
     });
}

create(e){
    e.preventDefault();
        
    
        
    let rezervacija = {
        naIme : this.state.naIme,
        brojOsoba : this.state.brojOsoba,
        datumIVreme : this.state.datumIVreme,
        sto : this.state.sto,
        korisnik : this.state.korisnik
        


    }

   
    TestAxios.post('/rezervacije' , rezervacija)
    .then(res => {
        console.log(res);

        alert('Uspesna rezervacija');
        this.props.history.push('/lokali');
        
    
    })

    .catch(error => {
        console.log(error);

        alert('Error! Neuspesna rezervacija!');
    });
}

getKorisnikByUserName(username){
    TestAxios.get('/korisnici/' + username)
    .then(res => {
        // handle success
        console.log(res.data);
        let korisnik = {
            id : res.data.id,
            korisnickoIme : res.data.korisnickoIme,
            eMail : res.data.eMail,
            ime : res.data.ime,
            prezime : res.data.prezime,
            rezervacije : res.data.rezervacije
        }
        this.setState({rezervacije : res.data.rezervacije});
        this.setState({korisnik : korisnik});
        
        
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Ulogujte se kako bi rezervisali sto!')
     });
}

setujKorisnika(){
    this.getKorisnikByUserName(window.localStorage['username']);


}



render(){
    return (
        <div className="d-flex justify-content-center">
        
        <Col xs="8" sm="6" md="6">
        <Form>
        <Form.Label id="zsto">Izabrani sto</Form.Label>
            <Form.Control  id="zsto" name="sto" readOnly value = {"Tip stola: " + this.state.sto.tip + "    Kapacitet stola: " +  this.state.sto.dozvoljenBrojLjudi +  "     Redni broj stola: " + this.state.sto.numerickiBroj}  /> <br/>
            <Form.Label id="zNaIme">Ime</Form.Label>
            <Form.Control placeholder="Unesite ime na koje zelite da glasi rezervacija" id="zNaIme" name="naIme" onChange={(e)=>this.naImeInputChanged(e)}/> <br/>
            <Form.Row>
            <Form.Group as={Col} controlId="formBrojOsoba">
            <Form.Label>Broj osoba</Form.Label>
            <Form.Control type="number" placeholder="Unesite broj osoba rezervacije" type="number" id="brojOsoba" name="brojOsoba" onChange={(e)=>this.brojOsobaInputChanged(e)}/> <br/>
            </Form.Group>
            <Form.Group as={Col} controlId="formDatumIVreme">
            <Form.Label id="zDatumIVreme">Datum i vreme</Form.Label>
            <Form.Control type="datetime-local" placeholder="Unesite datum i vreme rezervacije" id="zDatumIVreme" name="datumIVreme" onChange={(e)=>this.datumIVremeInputChanged(e)}/> <br/>
            </Form.Group>
            </Form.Row> 
           

            <Button type="submit" onClick={(e) => this.create(e)}>Rezervisi</Button>
        </Form>
        </Col>

        
       


      
 
    </div>

    

    )

}





}
export default Rezervisi;