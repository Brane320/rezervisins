import React from 'react';
import TestAxios from '../../apis/TestAxios';
import {Table,Button} from "react-bootstrap";
class RezervacijeKorisnika extends React.Component {

    constructor(props){
        super(props);
        let korisnik = {
            id : -1,
            korisnickoIme : "",
            eMail : "",
            ime : "",
            prezime : "",
            rezervacije : []
        }
        

    this.state = {korisnik : korisnik,rezervacije : []};
}

componentDidMount(){
    this.setujKorisnika();
    
 
}

getKorisnikByUserName(username){
    TestAxios.get('/korisnici/' + username)
    .then(res => {
        // handle success
        console.log(res.data);
        let korisnik = {
            id : res.data.id,
            korisnickoIme : res.data.korisnickoIme,
            eMail : res.data.eMail,
            ime : res.data.ime,
            prezime : res.data.prezime,
            rezervacije : res.data.rezervacije
        }
        this.setState({rezervacije : korisnik.rezervacije});
        this.setState({korisnik : korisnik});
        console.log(res.data.rezervacije);
      
        
        
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Korisnik nije dobijen')
     });
}

setujKorisnika(){
    this.getKorisnikByUserName(window.localStorage['username']);


}

delete(id) {
    TestAxios.delete('/rezervacije/' + id)
    .then(res => {
        // handle success
        console.log(res);
        alert('Rezervacija uspesno obrisana');
        window.location.reload();
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Problem sa brisanjem ');
     });
}


renderRezervacije() {
    return this.state.rezervacije.map((rezervacija) => {
        return (
           <tr key={rezervacija.id}>
              <td>{rezervacija.sto.lokal.naziv}</td>
              <td>{rezervacija.sto.tip}</td>
              <td>{rezervacija.brojOsoba}</td>
              <td>{rezervacija.naIme}</td>
              <td>
                {new Intl.DateTimeFormat('datetime-local', { 
                month: '2-digit', 
                day: '2-digit',
                year: 'numeric', 
                hour : 'numeric',
                minute : 'numeric'
            }).format(new Date(rezervacija.datumIVreme))}</td>
              <td>
                <Button variant="danger" onClick={() => this.delete(rezervacija.id)}>Otkazi rezervaciju</Button>
              </td>
              
            
              
              

           
           </tr>
        )
     })
}

render() {
    
    return (
        <div>
            
            
  
            
            <div>
                
                
            

               
                <h1>Rezervacije korisnika {window.localStorage['username']}</h1>
                
                <Table className="table table-dark" style={{marginTop:5}}>
                    <thead className="thead-dark">
                        <tr>
                            <th>Lokal rezervacije</th>
                            <th>Tip stola</th>
                            <th>Broj osoba rezervacije</th>
                            <th>Ime na koje glasi rezervacija</th>
                            <th>Datum i vreme rezervacije</th>
                            <th></th>
                            
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderRezervacije()}
                    </tbody>                  
                </Table>
                
                
            </div>
            

        </div>
    );
}



}

export default RezervacijeKorisnika;