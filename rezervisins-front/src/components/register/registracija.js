function validacija(){
	var korisnickoIme = $("input[type=text][name=korisnickoIme]").val()
	var lozinka = $("input[type=password][name=lozinka]").val()
	var ponovljenaLozinka = $("input[type=password][name=ponovljenaLozinka]").val()
	var eMail = $("input[type=text][name=eMail]").val()
	
	
	
	if(korisnickoIme == ""){
		$(".greska").text("Korisnicko ime ne sme biti prazno!")
		alert("Korisnicko ime ne sme biti prazno")
		return false
		
	}
	
	if (korisnickoIme.match("^[a-zA-Z0-9]+$") == null) {
		$(".greska").text("Korisnicko ime mora sadrzati samo alfanumericke karaktere")
		
		alert("Korisnicko ime mora sadrzati samo alfanumericke karaktere!")
		return false
	}
	
	if (lozinka == "") {
		$(".greska").text("Lozinka ne sme biti prazna")
		alert("Lozinka je prazna!")
		return false
	}
	
	if (lozinka != ponovljenaLozinka ) {
		$(".greska").text("Lozinke se moraju poklapati")
		alert("Lozinke se ne podudaraju")
		return false
	}
	
	if (eMail == "") {
		$(".greska").text("Email ne sme biti prazan")
		
		alert("Email je prazan")
		return false
	}
	
	if (eMail.match("^[a-zA-Z0-9]+@[a-zA-Z0-9]+.com$") == null) {
		$(".greska").text("Email mora biti u ispravnom formatu")
		alert("Email mora biti u ispravnom formatu")
		return false
	}
	
	
}

$(document).ready(function() {
	$("form:first").submit(validacija)
	
})