import React from 'react';
import {Table, Button,Col,Form} from 'react-bootstrap'
import TestAxios from '../../apis/TestAxios';

class Lokali extends React.Component {

    constructor(props) {
        super(props);

        this.state = { lokali: [],brojStranice : 0,totalPages : 0, naziv : null}
    }

    componentDidMount(){
        this.getLokale(0);
    }

    nazivInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;

        console.log(name + ", " + value);

        let naziv = this.state.naziv;

        naziv = value;
        

        this.setState({naziv : naziv})
        this.pretraga();


      }
     


      pretraga() {
        let params = {
            'naziv' : this.state.naziv
            


        }

        TestAxios.get('/lokali/pretraga', {params})
            .then(res => {
                 // handle success
                 console.log(res);
                 this.setState({lokali : res.data});
                 
                 
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Greska pri pretrazi');
            });
    }

    delete(id) {
        TestAxios.delete('/lokali/' + id)
        .then(res => {
            // handle success
            console.log(res);
            alert('Lokal uspesno obrisan');
            window.location.reload();
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Problem sa brisanjem ');
         });
    }





           

    getLokale(brojStranice) {
        let config = {
            params: {
              brojStranice: this.state.brojStranice,
              brojStranice : brojStranice
            },
          }

        TestAxios.get('/lokali', config)
            .then(res => {
                 // handle success
                 console.log(res);
                 this.setState({lokali: res.data,
                totalPages : res.headers["total-pages"],
                brojStranice : brojStranice
            });
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Greska pri ocitavanju lokala');
            });
    }

    goToStolovi(lokalId) {
        this.props.history.push('/stolovilokala/'+ lokalId); 
    }

    goToCreate(){
        this.props.history.push('/lokali/create');
    }

    

    renderLokale() {
        return this.state.lokali.map((lokal) => {
            return (
               <tr key={lokal.id}>
                  <td>{lokal.naziv}</td>
                  <td>{lokal.adresa}</td>
                  <td>{lokal.tip}</td>
                
                  <td> 
                  <Button style={{margin:3}} variant="warning" onClick={() => this.goToStolovi(lokal.id)} >Rezervisi svoje mesto</Button>
                  <Button hidden= {window.localStorage['uloga'] === "KORISNIK"}  style={{margin:3}} variant="danger" onClick={() => this.delete(lokal.id)}>Brisanje</Button>
                  
                  </td>
                  

               
               </tr>
            )
         })
    }

    render() {
        
        return (
            
    
           
       
            <div>
                <Col></Col>
            <Col xs="7" sm="5" md="5">
            <Form>
           
               
                
                <Form.Label htmlFor="im">Pretraga lokala:</Form.Label>
                <Form.Control placeholder="Unesite naziv lokala" id="im" name="naziv" onChange={(e)=>this.nazivInputChanged(e)}/> <br/>

                
               
                
            </Form>
            </Col>
                
                
                <div>
                   
                   
                    <Table  className="table table-dark"style={{marginTop:5}}>
                        <thead className="thead-dark">
                            <tr>
                                <th>Naziv lokala</th>
                                <th>Adresa</th>
                                <th>Tip</th>
                                
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderLokale()}
                        </tbody>                  
                    </Table>
                    <div style={{float:"right"}}><Button onClick={()=> this.goToCreate()}>Kreiraj novi lokal</Button></div> 
                    
                </div>
                <div class="btn-toolbar"><Button disabled={this.state.brojStranice==0} onClick={()=>this.getLokale(this.state.brojStranice -1)}>Prethodna</Button>
                    <Button disabled={this.state.brojStranice==this.state.totalPages-1} onClick={()=>this.getLokale(this.state.brojStranice +1)}>Sledeca</Button>
                </div>
               

            </div>
        );
    }




}

export default Lokali;
