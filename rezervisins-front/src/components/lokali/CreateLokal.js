import React from 'react';
import TestAxios from '../../apis/TestAxios';
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";

class CreateLokal extends React.Component {
    constructor(props){
        super(props);

    let lokal = {
        naziv: "",
        adresa: "",
        tip: "",
        
    }

    this.state = {lokal: lokal};
}

valueInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let lokal = this.state.lokal;
    lokal[name] = value;

    this.setState({lokal: lokal });
  }

  create(e) {
    let lokal = this.state.lokal;
    let lokalDTO = {
        naziv: lokal.naziv,
        adresa: lokal.adresa,
        tip: lokal.tip,

    }

    TestAxios.post('/lokali', lokalDTO)
    .then(res => {
        console.log(res);

        alert('Uspesno dodat lokal');
        this.props.history.push("/lokali");
    })

    .catch(error => {
        console.log(error);

        alert('Error! Neuspesno kreiranje lokala!');
    });

}

render(){
    return (
        <>
        <Row>
        <Col></Col>
        <Col xs="10" sm="8" md="8">
        <Form>
            <Form.Label htmlFor="zNaziv">Naziv</Form.Label>
            <Form.Control placeHolder="Ovde unesite naziv novog lokala" id="zNaziv" name="naziv" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zAdresa">Adresa</Form.Label>
            <Form.Control placeHolder="Ovde unesite adresu novog lokala" id="zAdresa" name="adresa" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label id="zTip">Tip</Form.Label>
            <Form.Control placeHolder="Ovde unesite tip novog lokala" id="zTip" name="tip" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            

          
           

       
            <Button style={{ marginTop: "25px" }}onClick={(e) => this.create(e)}>Kreiraj lokal</Button>
        </Form>
        </Col>

        
        <Col></Col>
        </Row>


      
 
    </>

    

    )
}
}

export default CreateLokal;