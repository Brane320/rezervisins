import React from 'react';
import TestAxios from '../../apis/TestAxios';
import {Form, Button, Card } from "react-bootstrap";
class Komentari extends React.Component {

    constructor(props){
        super(props);
        let lokal = {
            id : -1,
            naziv: "",
            adresa : "",
            tip : "",
            komentari: ""
        }

    this.state = {id : 0, lokal : lokal, komentari : []};
}

componentDidMount(){
    this.getLokalById(this.props.match.params.id);
    
 
}

goToCreate(lokalId){
    this.props.history.push('/komentari/create/' + lokalId);
}

getLokalById(lokalId) {
    TestAxios.get('/lokali/' + lokalId)
    .then(res => {
        // handle success
        console.log(res.data);
        let lokal = {
            id : res.data.id,
            naziv: res.data.naziv,
            adresa : res.data.adresa,
            tip : res.data.tip,
            komentari : res.data.komentari
        }
        this.setState({lokal : lokal});
        this.setState({komentari : res.data.komentari});
        
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Komentari nisu ucitani')
     });
}

renderKomentare() {
    return this.state.komentari.map((komentar) => {
       
        return (
            <section>
        <div >
        <div className="row">
            <div className="col-sm-4 col-md-5 col-10 pb-2">
                 <div className="p-2 mb-1 bg-dark text-white">
                    <h4>{komentar.imeAutora}</h4> <span> {new Intl.DateTimeFormat('datetime-local', { 
                month: '2-digit', 
                day: '2-digit',
                year: 'numeric', 
            }).format(new Date(komentar.datumPostavljanja))}</span> <br/>
                    <p>{komentar.tekst}</p>
                </div>
        </div>
        </div>
        </div>
        
             </section>

        
          
              
        
        
        )
        
     })
    
}

render() {
    
    return (
        <div>
                <div>
                <h1>{this.state.lokal.naziv} - Komentari o lokalu</h1>
                <Button style={{ marginBottom: "10px" }} onClick={() => this.goToCreate(this.state.lokal.id)}>Dodaj svoj komentar</Button>
                {this.renderKomentare()}
                </div>
         </div>
    );
}


}

export default Komentari;