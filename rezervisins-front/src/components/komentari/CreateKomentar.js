import React from 'react';
import TestAxios from '../../apis/TestAxios';
import {Form, Button, Row, Col, InputGroup,Table } from "react-bootstrap";
class CreateKomentar extends React.Component {

    constructor(props){
        super(props);
        let lokal = {
            id : -1,
            naziv: "",
            adresa : "",
            tip : "",
            
        }

    this.state = {id : 0,imeAutora : "", tekst : "", lokal : lokal};
}

componentDidMount(){
    this.getLokalById(this.props.match.params.id);
}

getLokalById(lokalId) {
    TestAxios.get('/lokali/' + lokalId)
    .then(res => {
        // handle success
        console.log(res.data);
        let lokal = {
            id : res.data.id,
            naziv: res.data.naziv,
            adresa : res.data.adresa,
            tip : res.data.tip,
            
        }
        this.setState({lokal : lokal});
        
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Neuspesno dobijen lokal')
     });
}

imeAutoraInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    
    let imeAutora = this.state.imeAutora;

    imeAutora = value;

    this.setState({imeAutora : imeAutora});
  }

  tekstInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    
    let tekst = this.state.tekst;

    tekst = value;

    this.setState({tekst : tekst});
  }

  create(e) {
    
    let komentarDTO = {
        imeAutora : this.state.imeAutora,
        tekst : this.state.tekst,
        lokal : this.state.lokal
    }

    TestAxios.post('/komentari', komentarDTO)
    .then(res => {
        console.log(res);

        alert('Uspesno poslat komentar!');
        this.props.history.push('/komentarilokala/' + this.state.lokal.id);
        
    })

    .catch(error => {
        console.log(error);

        alert('Error! Neuspesno postavljanje komentara!');
    });

}

render(){
    return (
        <>
        <Row>
        <Col></Col>
        
        <Col xs="10" sm="8" md="8">
        <Form>
            <Form.Label>Vaše ime</Form.Label>
            <Form.Control placeHolder="Unesite ime" id="imeAutora" name="imeAutora" onChange={(e)=>this.imeAutoraInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zDozvoljeno">Vaš Komentar</Form.Label>
            <Form.Control as="textarea" placeHolder="Unesite komentar o ovom lokalu" row id="komentar" name="tekst" onChange={(e)=>this.tekstInputChanged(e)}/> <br/>
            
            

            
           

       
            <Button style={{ marginTop: "25px" }}onClick={(e) => this.create(e)}>Dodaj komentar</Button>
        </Form>
        </Col>

        
        <Col></Col>
        </Row>


      
 
    </>

    

    )
}






}

export default CreateKomentar;