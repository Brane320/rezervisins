import React from 'react';
import TestAxios from '../../apis/TestAxios';
import {Form, Button, Row, Col, InputGroup,Table } from "react-bootstrap";
class CreateSto extends React.Component {

    constructor(props){
        super(props);
        let lokal = {
            id : -1,
            naziv: "",
            adresa : "",
            tip : "",
            
        }

    this.state = {id : 0,numerickiBroj : 0, dozvoljenBrojLjudi : 0, tip : "", lokal : lokal};
}

componentDidMount(){
    this.getLokalById(this.props.match.params.id);
    
 
}
getLokalById(lokalId) {
    TestAxios.get('/lokali/' + lokalId)
    .then(res => {
        // handle success
        console.log(res.data);
        let lokal = {
            id : res.data.id,
            naziv: res.data.naziv,
            adresa : res.data.adresa,
            tip : res.data.tip,
            
        }
        this.setState({lokal : lokal});
        
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Neuspesno dobijen lokal')
     });
}
tipInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    
    let tip = this.state.tip;

    tip = value;

    this.setState({tip : tip });
  }
  numerickiInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let numerickiBroj = this.state.numerickiBroj;

    numerickiBroj = value;

    this.setState({numerickiBroj : numerickiBroj});
        
  }
  dozvoljenBrojInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let dozvoljenBrojLjudi = this.state.dozvoljenBrojLjudi;

    dozvoljenBrojLjudi = value;

    this.setState({dozvoljenBrojLjudi : dozvoljenBrojLjudi});
    


    
  }

  create(e) {
    
    let stoDTO = {
        numerickiBroj : this.state.numerickiBroj,
        dozvoljenBrojLjudi : this.state.dozvoljenBrojLjudi,
        tip : this.state.tip,
        lokal : this.state.lokal

    }

    TestAxios.post('/stolovi', stoDTO)
    .then(res => {
        console.log(res);

        alert('Uspesno dodat sto!');
        this.props.history.push('/stolovilokala/' + this.state.lokal.id);
        
    })

    .catch(error => {
        console.log(error);

        alert('Error! Neuspesno kreiranje stola!');
    });

}
render(){
    return (
        <>
        <Row>
        <Col></Col>
        
        <Col xs="10" sm="8" md="8">
        <Form>
            <Form.Label htmlFor="zNumericki">Numericki broj</Form.Label>
            <Form.Control placeHolder="Unesite numericki broj stola" type="number" id="zNumericki" name="numerickiBroj" onChange={(e)=>this.numerickiInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zDozvoljeno">Dozvoljen broj ljudi</Form.Label>
            <Form.Control placeHolder="Unesite dozvoljen broj ljudi za stolom" type="number" id="zDozvoljeno" name="dozvoljenBrojLjudi" onChange={(e)=>this.dozvoljenBrojInputChanged(e)}/> <br/>
            <Form.Label id="zTip">Tip</Form.Label>
            <Form.Control placeHolder="Unesite tip stola"  id="zTip" name="tip" onChange={(e)=>this.tipInputChanged(e)}/> <br/>
            

            
           

       
            <Button style={{ marginTop: "25px" }}onClick={(e) => this.create(e)}>Dodaj sto lokalu {this.state.lokal.naziv}</Button>
        </Form>
        </Col>

        
        <Col></Col>
        </Row>


      
 
    </>

    

    )
}



}

export default CreateSto;