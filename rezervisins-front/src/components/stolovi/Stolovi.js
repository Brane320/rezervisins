import React from 'react';
import TestAxios from '../../apis/TestAxios';
import {Form, Button, Row, Col, InputGroup,Table } from "react-bootstrap";
import BootstrapSwitchButton from 'bootstrap-switch-button-react';
import raspored from "../../raspored.jpg";
class Stolovi extends React.Component {


    constructor(props){
        super(props);
        let lokal = {
            id : -1,
            naziv: "",
            adresa : "",
            tip : "",
            stolovi: ""
        }

    this.state = {id : 0, lokal : lokal, stolovi : [], prikaziRaspored : false};
}

componentDidMount(){
    this.getLokalById(this.props.match.params.id);
    
}






getLokalById(lokalId) {
    TestAxios.get('/lokali/' + lokalId)
    .then(res => {
        // handle success
        console.log(res.data);
        let lokal = {
            id : res.data.id,
            naziv: res.data.naziv,
            adresa : res.data.adresa,
            tip : res.data.tip,
            stolovi: res.data.stolovi
        }
        this.setState({lokal : lokal});
        this.setState({stolovi : res.data.stolovi});
        
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Stolovi nisu ucitani! Ulogujte se da bi videli slobodne stolove!')
     });
}

goToRez(stoId) {
    this.props.history.push('/rezervisi/'+ stoId); 
}

goToCreate(lokalId){
    this.props.history.push('/stolovi/create/' + lokalId);
}


renderStolove() {
    return this.state.stolovi.map((sto) => {
        return (
           <tr key={sto.id}>
              <td>{sto.numerickiBroj}</td>
              <td>{sto.dozvoljenBrojLjudi}</td>
              <td>{sto.tip}</td>
            
              <td> 
              <Button  disabled={sto.rezervisan==true} onClick={() => this.goToRez(sto.id)} style={{margin:3}} variant="warning">Rezervisi sto</Button>
              </td>
              

           
           </tr>
        )
     })
}

goToKomentari(lokalId) {
    this.props.history.push('/komentarilokala/'+ lokalId); 
}

prikaziSlikuRasporeda(){
    return (
        <div className="mt-1">
            <img className="rounded mx-auto d-block" height="475" width="1100" src={raspored} alt="slikaRasporeda"></img>
        </div>
    )
}

prikaziRaspored(){
    this.setState({prikaziRaspored : !this.state.prikaziRaspored});
}

render() {
    
    return (
        <div>
             <div>
                
                <h1>{this.state.lokal.naziv} - Izbor stolova</h1>
                <Col></Col>
                <p>Napomena: Ukoliko je dugme REZERVISI onemoguceno znaci da je sto vec rezervisan.</p>
                
                <Button style={{marginRight:200}} onClick={() => this.goToKomentari(this.state.lokal.id)} >Pogledaj komentare o ovom lokalu</Button>
                
                <BootstrapSwitchButton onChange={()=>this.prikaziRaspored()} onlabel='Vrati se' offlabel='Prikaži raspored' offstyle='primary' width='180'/>
               
                <div style={{float:"right"}}><Button onClick={()=> this.goToCreate(this.state.lokal.id)}>Dodaj novi sto ovom lokalu</Button></div>
                {this.state.prikaziRaspored ? this.prikaziSlikuRasporeda() :
                <Table className="table table-dark" style={{marginTop:5}}>
                    <thead className="thead-dark">
                        <tr>
                            <th>Numericki broj stola</th>
                            <th>Dozvoljen broj ljudi</th>
                            <th>Tip stola</th>
                            
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderStolove()}
                    </tbody>                  
                </Table>    }

             </div>
        </div>
    );
}




}

export default Stolovi;