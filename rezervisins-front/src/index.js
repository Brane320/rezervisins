import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Link, Switch, Route } from 'react-router-dom';
import { Navbar, Nav, Container,Button } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from "./components/login/Login"
import Register from "./components/register/Register"
import {logout} from './services/auth'
import './index.css'
import Home from './Home';
import Lokali from './components/lokali/Lokali';
import Stolovi from './components/stolovi/Stolovi';
import Rezervisi from './components/rezervacije/Rezervisi';
import CreateLokal from './components/lokali/CreateLokal';
import CreateSto from './components/stolovi/CreateSto';
import RezervacijeKorisnika from './components/rezervacije/RezervacijeKorisnika';
import Komentari from './components/komentari/Komentari';
import CreateKomentar from './components/komentari/CreateKomentar';





class App extends React.Component {
  

    render() {
        return (
            
          <div>
          <Router>
            <Navbar expand bg="dark" variant="dark">
              <Navbar.Brand as={Link} to="/">
                  Početna stranica
              </Navbar.Brand>
             
              
              <Nav>
                <Nav.Link as={Link} to="/lokali">
                  Prikaz svih lokala
                </Nav.Link>
              </Nav>

                <Nav className="ml-auto">

                {window.localStorage['jwt'] ? 
                          <Button hidden onClick = {()=>logout()}>Logout</Button> :
                          <Nav.Link as={Link} to="/register">Registracija</Nav.Link>}


                 {window.localStorage['jwt'] ? 
                          <Button onClick = {()=>logout()}>Logout</Button> :
                          <Nav.Link as={Link} to="/login">Login</Nav.Link>}
                 
                {window.localStorage['jwt'] ? 
                <Nav.Link> Ulogovani korisnik: {window.localStorage['username']}</Nav.Link>  : ""}   
                
                {window.localStorage['jwt'] ? 
               <Nav.Link as={Link} to="/rezervacijekorisnika">Rezervacije korisnika </Nav.Link> : ""}   

            </Nav>
            </Navbar>
              <Container style={{paddingTop:"25px"}}>
                <Switch>
                  
                <Route exact path="/" component={Home}/> 
                <Route exact path="/login" component={Login}/>
                <Route exact path="/register" component={Register}/>
                <Route exact path="/lokali" component={Lokali}/>
                <Route exact path="/stolovilokala/:id" component={Stolovi}/>
                <Route exact path="/rezervisi/:id" component={Rezervisi}/>
                <Route exact path="/lokali/create" component={CreateLokal}/>
                <Route exact path="/stolovi/create/:id" component={CreateSto}/>
                <Route exact path="/komentari/create/:id" component={CreateKomentar}/>
                <Route exact path="/rezervacijekorisnika" component={RezervacijeKorisnika}/>
                <Route exact path="/komentarilokala/:id" component={Komentari}/>

                </Switch>
              </Container>
            </Router>
            
          </div>
        )
    }
};

ReactDOM.render(
    <App/>,
    document.querySelector('#root')
);